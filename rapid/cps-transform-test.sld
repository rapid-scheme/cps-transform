;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid cps-transform-test)
  (export run-tests)
  (import (scheme base)
	  (scheme eval)
	  (rapid test)
	  (rapid list)
	  (rapid syntax)
	  (rapid cps-transform))
  (begin
    (define test-env (environment '(scheme base) '(scheme case-lambda) '(rapid receive)))
    (define (test-eval expr)
      (eval `(let ((a 11)
		   (b 20)
		   (c 30)
		   (t #t)
		   (f #f))
	       ,(syntax->datum expr))
	    test-env))

    (define-syntax test-cps
      (syntax-rules ()
	((test-cps name expr)
	 (test-group name
           (let ((%expr (syntax ,expr)))
	     (let ((transform (cps-transform %expr)))
	       (test-assert (cps? transform))
	       (test-equal (test-eval %expr) (test-eval transform))))))))
    
    (define (cps? expr)
      (syntax-match expr
	((if ,t ,c ,a)
	 (and (atomic? t) (cps? c) (cps? a)))
	((case-lambda (,formals* ,body*) ...)
	 (every cps? body*))
	((letrec ((,var* ,init*) ...) ,body)
	 (and (every cps? init*) (cps? body)))
	((apply ,operand* ...)
	 (every atomic? operand*))
	((receive ,formals (,operator ,operand* ...) ,body)
	 (and (every atomic? operand*) (cps? body)))
	(,x
	 (guard (atomic? x))
	 #t)))

    (define (run-tests)
      (test-begin "CPS-transformation")

      (test-group "Examples"
        (test-cps "Example 1"
		  '(+ (- '10 a) (* '2 '3)))

	(test-cps "Example 2"
		  '(letrec ((g (case-lambda
				 ((x y) (+ x y)))))
		     (apply g (* a b) c '())))
	
	(test-cps "Example 3"
		  '(letrec ((g (case-lambda
				 ((x)
				  (case-lambda
				    ((c)
				     (apply c x '())))))))
		     (call/cc (apply g '1 '()))))

	(test-cps "Example 4"
		  '(procedure? (case-lambda ((a) (apply g a '())) (b (apply g b)))))

	(test-cps "Example 5"
		  '(letrec ((e (case-lambda
				 (() (values '1)))))
		     (if (let-values (((a) (apply e '()))) a) t f)))

	(test-cps "Example 6"
		  '(let-values (((x y z) (values '1 '2 '3))) (+ x y z)))

	(test-cps "Example 7"
		  '(+ (begin a b c))))

      (test-end))
      
    (define (atomic? expr)
      (syntax-match expr
	((case-lambda ,clause* ...)
	 #t)
	((quote ,literal)
	 #t)
	(,x
	 (guard (symbol? (unwrap-syntax x)))
	 #t)
	(,_
	 #f)))))
