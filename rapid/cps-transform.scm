;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> \procedure{(cps-transform expr)}

;;> Performs a CPS-transformation of \var{expr}.  It takes a syntax
;;> object encapsulating an expression of the input language and
;;> returns a syntax object encapsulating an expression of the output
;;> language.

;;> The input language has the following expressions: \scheme{(begin
;;> ...)}, \scheme{(if ...)}, \scheme{(quote ...)},
;;> \scheme{(case-lambda ...)}, \scheme{(letrec ...)},
;;> \scheme{(let-values ...)}  \scheme{(receive ...)}, \scheme{(apply
;;> ...)}, \scheme{(call/cc ...)}, primitive operations, and variable references.

;;> The output language has the following expressions:
;;> \scheme{(if ...)}, \scheme{(quote ...)},
;;> \scheme{(case-lambda ...)}, \scheme{(letrec ...)},
;;> \scheme{(receive ...)}, \scheme{(apply ...)} in tail position, and
;;> variable references.

(define (cps-transform expr)
  (%cps-transform expr values))

(define (%cps-transform expr k)
  (syntax-match expr
    ;; begin
    ((begin ,e)
     (%cps-transform e k))
    ((begin ,e ,e* ...)
     (call-with-cps-transform e
       (lambda (a)
	 (%cps-transform (syntax (begin ,e* ...)) k))))

    ;; if
    ((if ,test ,consequent ,alternate)
     (let ((c (gensym 'cont)))
       (call-with-syntactic-continuation k
	 (lambda (k)
	   (call-with-cps-transform test
	     (lambda (a)
	       (syntax
		(apply
		 (case-lambda
		   ((,c) (if ,a
			     ,(%cps-transform consequent c)
			     ,(%cps-transform alternate c))))
		 ,k '()))))))))

    ;; quote
    ((quote ,literal)
     (return k (syntax (quote ,literal))))
    
    ;; case-lambda
    ((case-lambda (,formals* ,body*) ...)
     (let*
	 ((c* (map (lambda (formals)
		     (gensym 'cont))
		   formals*))
	  (body* (map %cps-transform body* c*)))
       (return k
	       (syntax (case-lambda
			 ((,c* . ,formals*) ,body*) ...)))))
    
    ;; letrec
    ((letrec ((,var* ,init*) ...) ,expr)
     (call-with-cps-transform* init*
       (lambda (a*)
	 (syntax (letrec ((,var* ,a*) ...)
		   ,(%cps-transform expr k))))))
    
    ;; let-values
    ((let-values ((,formals ,expr)) ,body)
     (let ((c (gensym 'cont)))
       (syntax (letrec ((,c (case-lambda
			     (,formals ,(%cps-transform body k)))))
		 ,(%cps-transform expr c)))))

    ;; values
    ((values ,expr* ...)
     (call-with-cps-transform* expr*
       (lambda (a*)
	 (apply return k a*))))

    ;; call/cc
    ((call/cc ,proc)
     (let ((v (gensym 'value))
	   (c (gensym 'cont)))
       (call-with-syntactic-continuation k
	 (lambda (k)
	   (call-with-cps-transform proc
	     (lambda (f)
	       (syntax (apply ,f ,k (case-lambda
				      ((,c ,v)
				       (apply ,k ,v '())))
			      '()))))))))
    
    ;; apply
    ((apply ,operand* ...)
     (call-with-syntactic-continuation k	 
       (lambda (k)
	 (call-with-cps-transform* operand*
	   (lambda (a*)
	     (syntax (apply ,(car a*)
			    ,k
			    . ,(cdr a*))))))))
    
    ;; primitives calls
    ((receive ,formals
	 (,operator ,operand* ...)
       ,expr)
     (call-with-cps-transform* operand*
       (lambda (a*)
	 (syntax (receive ,formals
		     (,operator ,a* ...)
		   ,(%cps-transform expr k))))))
    ((,operator ,operand* ...)
     (let ((v (gensym 'value)))
       (%cps-transform (syntax (receive (,v) (,operator ,operand* ...) ,v)) k)))
    
    ;; references
    (,expr
     (return k expr))))

(define (return k . e*)
  (if (procedure? k)
      (cond
       ((and (not (null? e*)) (null? (cdr e*)))
	(k (car e*)))
       (else
	(raise-syntax-error (current-syntax-context)
			    "too few or too many values")
	(k (syntax (if #f #f)))))
      (syntax (apply ,k ,e* ... '()))))

(define (call-with-syntactic-continuation k proc)
  (if (procedure? k)
      (let ((v (gensym 'value))
	    (c (gensym 'cont)))
	(syntax
	 (letrec ((,c (case-lambda
			((,v) ,(k v)))))
	   ,(proc c))))
      (proc k)))

(define (call-with-cps-transform e proc)
  (call-with-cps-transform* (list e)
    (lambda (a*)
      (proc (car a*)))))

(define (call-with-cps-transform* e* proc)
  (let ((context (current-syntax-context)))
    (let loop ((e*
		e*)
	       (k
		(lambda (a*)
		  (parameterize ((current-syntax-context context))
		    (proc a*)))))
      (if (null? e*)
	  (return k '())
	  (%cps-transform (car e*)
			  (lambda (a)
			    (loop (cdr e*)
				  (lambda (a*)
				    (return k (cons a a*))))))))))

;; Local Variables:
;; eval: (put 'call-with-syntactic-continuation 'scheme-indent-function 1)
;; eval: (put 'call-with-cps-transform 'scheme-indent-function 1)
;; eval: (put 'call-with-cps-transform* 'scheme-indent-function 1)
;; eval: (font-lock-add-keywords 'scheme-mode
;;                               '(("(\\(call-with-syntactic-continuation\\)\\>" 1 font-lock-keyword-face)
;;                                 ("(\\(call-with-cps-transform\\)\\>" 1 font-lock-keyword-face)
;;                                 ("(\\(call-with-cps-transform*\\)\\>" 1 font-lock-keyword-face)))
;; End:
